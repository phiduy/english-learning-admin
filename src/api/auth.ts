import { request } from '../config/axios';
import { IUserRegisterInfo } from '../modules/RegisterPage';
import { IUserLoginInfo } from '../modules/LoginPage';

export const login = (data: IUserLoginInfo) => {
	const endpoint = '/users/signin';
	return request(endpoint, 'POST', data);
};

export const register = (data: IUserRegisterInfo) => {
	const endpoint = '/users/signup';
	return request(endpoint, 'POST', data);
};

export const resetPassword = (data: { email: string }) => {
	const endpoint = '/users/resetPassword';
	return request(endpoint, 'POST', data);
};

export const adminChangePassword = (data: { email: string; password: string }) => {
	const endpoint = '/users/admin/changePassword';
	return request(endpoint, 'PATCH', data);
};
