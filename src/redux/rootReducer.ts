/**
 * @file Root reducers
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import IStore from './store/IStore';

// Place for reducers' app
import RegisterPage, { name as nameOfRegisterPage } from '../modules/RegisterPage';
import LoginPage, { name as nameOfLoginPage } from '../modules/LoginPage';
import MainLayout, { name as nameOfMainLayout } from '../layouts/MainLayout';

/*----Reducers List-----------------*/
const reducers = {
	[nameOfRegisterPage]: RegisterPage,
	[nameOfLoginPage]: LoginPage,
	[nameOfMainLayout]: MainLayout,
};

const rootReducer = (history: History) =>
	combineReducers<IStore>({
		...reducers,
		router: connectRouter(history),
	});

export default rootReducer;
