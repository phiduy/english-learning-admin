import { initialState as LoginInitialState, name as LoginPage } from '../../modules/LoginPage';
import {
	initialState as RegisterInitialState,
	name as RegisterPage,
} from '../../modules/RegisterPage';
import {
	initialState as MainLayoutInitialState,
	name as MainLayout,
} from '../../layouts/MainLayout';
import IStore from './IStore';

export const initialState: IStore = {
	[LoginPage]: LoginInitialState,
	[RegisterPage]: RegisterInitialState,
	[MainLayout]: MainLayoutInitialState,
	router: null,
};

export default initialState;
