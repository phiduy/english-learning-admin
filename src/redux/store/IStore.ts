import { ILogInState, name as LoginPageState } from './../../modules/LoginPage';
import { IRegisterState, name as RegisterPageState } from './../../modules/RegisterPage';
import { IMainLayoutState, name as MainLayoutState } from '../../layouts/MainLayout';

export default interface IStore {
	[LoginPageState]: ILogInState;
	[RegisterPageState]: IRegisterState;
	[MainLayoutState]: IMainLayoutState;
	router: any;
}
