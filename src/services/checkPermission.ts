import { store } from '../index';
import { getLocalStorage } from './localStorage';

export const isGranted = (permissionKey: string) => {
	const acceptedRole: string[] = store.getState().LoginPage.acceptedRole;
	if (acceptedRole.length > 0) {
		return acceptedRole.includes(permissionKey);
	} else {
		const storedRole = getLocalStorage('acceptedRole');
		if (storedRole !== null) {
			return JSON.parse(storedRole).includes(permissionKey);
		}
		return false;
	}
};
