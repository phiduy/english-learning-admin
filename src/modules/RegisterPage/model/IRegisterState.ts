export interface IUserRegisterInfo {
	firstName: string;
	lastName: string;
	email: string;
	password: string;
}

export interface IRegisterState {
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: IRegisterState = {
	isLoading: false,
	isProcessing: false,
};
