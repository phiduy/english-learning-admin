import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';
import { push } from 'connected-react-router';

// Handle User Register
function* handleUserRegister(action: any) {
	try {
		const res = yield call(AuthApi.register, action.payload);
		yield delay(500);
		if (res.status === 200 || res.status === 201) {
			message.success('Register successful', 2);
			yield put(actions.userRegisterSuccess(res));
			yield put(push('/login'));
		} else {
			const { error } = res.data;
			message.error(error.message, 3);
			yield put(actions.userRegisterFail(res));
		}
	} catch (error) {
		yield put(actions.userRegisterFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserRegister() {
	yield takeEvery(Keys.USER_REGISTER, handleUserRegister);
}
/*-----------------------------------------------------------------*/
const sagas = [watchUserRegister];

export default sagas;
