import { IPermission } from '../../../common/interfaces';

export interface IUserLoginInfo {
	email: string;
	password: string;
}

export interface ILogInState {
	accessToken: string | any;
	refreshToken: string | any;
	role: string | any;
	acceptedRole: string[] | any;
	rolePermissions: IPermission[] | any;
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: ILogInState = {
	role: localStorage.getItem('role') || null,
	rolePermissions: null,
	acceptedRole: localStorage.getItem('acceptedRole') || null,
	accessToken: localStorage.getItem('accessToken') || null,
	refreshToken: localStorage.getItem('refreshToken') || null,
	isLoading: false,
	isProcessing: false,
};
