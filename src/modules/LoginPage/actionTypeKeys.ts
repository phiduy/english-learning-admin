/**
 * @file  Key of action will be listed here
 */

enum ActionTypeKeys {
	HANDLE_CLEAR = 'LOGIN_PAGE/HANDLE_CLEAR',
	USER_LOGIN = 'LOGIN_PAGE/USER_LOGIN',
	USER_LOGIN_SUCCESS = 'LOGIN_PAGE/USER_LOGIN_SUCCESS',
	USER_LOGIN_FAIL = 'LOGIN_PAGE/USER_LOGIN_FAIL',
}

export default ActionTypeKeys;
