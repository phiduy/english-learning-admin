/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IUserLoginInfo } from './model/ILoginState';

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: string;
	};
}

//#region User Login IActions
export interface IUserLogin extends Action {
	readonly type: Keys.USER_LOGIN;
	payload: IUserLoginInfo;
}

export interface IUserLoginSuccess extends Action {
	readonly type: Keys.USER_LOGIN_SUCCESS;
	payload: {
		token: string;
		role: string;
	};
}

export interface IUserLoginFail extends Action {
	readonly type: Keys.USER_LOGIN_FAIL;
	payload?: {
		message: string;
	};
}
//#endregion
