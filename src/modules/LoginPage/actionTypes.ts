/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IUserLogin
	| IActions.IUserLoginSuccess
	| IActions.IUserLoginFail;

export default ActionTypes;
