/**
 * @file  Key of action will be listed here
 */

enum ActionTypeKeys {
	TOGGLE_MODAL = 'MAIN_LAYOUT/TOGGLE_MODAL',
	HANDLE_LOGOUT = 'MAIN_LAYOUT/HANDLE_LOGOUT',
	ADMIN_CHANGE_USER_PASSWORD = 'MAIN_LAYOUT/ADMIN_CHANGE_USER_PASSWORD',
	ADMIN_CHANGE_USER_PASSWORD_SUCCESS = 'MAIN_LAYOUT/ADMIN_CHANGE_USER_PASSWORD_SUCCESS',
	ADMIN_CHANGE_USER_PASSWORD_FAIL = 'MAIN_LAYOUT/ADMIN_CHANGE_USER_PASSWORD_FAIL',
}
export default ActionTypeKeys;
