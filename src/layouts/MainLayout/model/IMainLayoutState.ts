export enum MAIN_LAYOUT_MODAL {
	CHANGE_USER_PASSWORD = 1,
}

export interface IMainLayoutState {
	toggleModalChangeUserPassword: boolean;
	isProcessing: boolean;
	email: string;
	password: string;
}

// InitialState
export const initialState: IMainLayoutState = {
	toggleModalChangeUserPassword: false,
	isProcessing: false,
	email: '',
	password: '',
};
