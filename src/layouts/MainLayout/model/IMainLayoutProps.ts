import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';
import {} from './IMainLayoutState';

export interface IMainLayoutProps {
	store: IStore;
	actions: typeof Actions;
}
