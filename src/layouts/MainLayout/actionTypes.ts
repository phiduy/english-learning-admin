/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleLogout
	| IActions.IAdminChangeUserPassword
	| IActions.IAdminChangeUserPasswordSuccess
	| IActions.IAdminChangeUserPasswordFail;

export default ActionTypes;
