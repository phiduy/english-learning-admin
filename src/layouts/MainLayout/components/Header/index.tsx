import React from 'react';
import { Menu, Layout } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import './index.scss';
import { Link } from 'react-router-dom';
import { IMainLayoutProps } from '../../model/IMainLayoutProps';
import { MAIN_LAYOUT_MODAL } from '../../model/IMainLayoutState';

const { SubMenu } = Menu;
const { Header } = Layout;
export const HeaderBar = (props: IMainLayoutProps) => {
	const [selectedKey, setSelectedKey] = React.useState('shop');

	const { role } = props.store.LoginPage;

	const handleClick = (e: any) => {
		if (e.key === 'changePassword') {
			props.actions.toggleModal({
				type: MAIN_LAYOUT_MODAL.CHANGE_USER_PASSWORD,
			});
		} else {
			setSelectedKey(e.key);
		}
	};
	return (
		<React.Fragment>
			<div className="header_dash__container">
				<Header>
					<Menu
						theme="light"
						onClick={handleClick}
						selectedKeys={[selectedKey]}
						mode="horizontal"
					>
						<Menu.Item key="shop">
							<Link to="/shop">Wish - Admin</Link>
						</Menu.Item>
						{role !== null && (
							<Menu.Item key="changePassword">
								<span>Change User Password</span>
							</Menu.Item>
						)}
						<span className="mr-auto" />
						<SubMenu key="account" icon={<UserOutlined />}>
							<Menu.Item key="setting:4" onClick={() => props.actions.handleLogout()}>
								Log out
							</Menu.Item>
						</SubMenu>
					</Menu>
				</Header>
			</div>
		</React.Fragment>
	);
};
