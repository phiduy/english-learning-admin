import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Layout, BackTop } from 'antd';
import { IMainLayoutProps } from '../model/IMainLayoutProps';
import { RouteConfig } from '../../../routes';
import { HeaderBar } from './Header';
import { ChangeUserPassword } from './Form';

import './MainLayout.scss';

const { Content } = Layout;

interface IProps extends IMainLayoutProps {
	routes: RouteConfig[];
}
export class MainLayout extends React.Component<IProps> {
	render() {
		const { routes } = this.props;

		return (
			<React.Fragment>
				<HeaderBar {...this.props} />
				<Layout className="workspace">
					<Layout>
						<Content>
							<div className="page__inner">
								<Switch>
									{routes.map((item) => {
										return (
											<Route
												key={item.path}
												path={item.path}
												component={item.component}
											/>
										);
									})}
									{this.props.routes.length > 0 ? (
										<Redirect to={this.props.routes[0].path} />
									) : null}
									<Redirect from="*" to="/404" />
								</Switch>
								{this.props.children}
							</div>
						</Content>
					</Layout>
					<BackTop />
				</Layout>
				<ChangeUserPassword {...this.props} />
			</React.Fragment>
		);
	}
}

export default MainLayout;
